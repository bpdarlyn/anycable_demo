lock "~> 3.10.0"

set :repo_url,        'git@bitbucket.org:bpdarlyn/anycable_demo.git'
set :application,     'anycable_demo'
set :user,            'vagrant'
set :rvm_type, :auto                     # Defaults to: :auto
set :rvm_ruby_version, '2.3.1@anycable_demo'
