# Load the Rails application.
require_relative 'application'

require 'faker/food'
require 'anycable-rails'

# Initialize the Rails application.
Rails.application.initialize!
