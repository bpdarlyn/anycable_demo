# Be sure to restart your server when you modify this file. Action Cable runs in a loop that does not support auto reloading.
module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :current_user

    def connect
      self.current_user = verify_user
    end

    def disconnect
      ActionCable.server.broadcast(
        "notifications",
        type: 'alert', data: "#{current_user} disconnected"
      )
    end

    private

    def verify_user
      puts 'HERE AM I ?'
      puts cookies[:username]
      cookies[:username].presence || reject_unauthorized_connection
    end
  end
end
